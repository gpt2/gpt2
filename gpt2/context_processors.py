from django.core.urlresolvers import resolve

def add_namespace_processor(request):
    return {'current_app': resolve ( request.path_info ) . app_name}

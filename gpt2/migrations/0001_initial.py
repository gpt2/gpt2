# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gpt2.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Candidat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('civilite', models.CharField(max_length=255, null=True)),
                ('prenom', models.CharField(max_length=255)),
                ('nom', models.CharField(max_length=255)),
                ('photo', models.ImageField(null=True, upload_to=gpt2.models.get_image_path, blank=True)),
                ('additional_position_in_list', models.IntegerField()),
                ('additional_nom_liste', models.CharField(max_length=128)),
            ],
            options={
                'db_table': 'candidat',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Circonscription',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('numero', models.IntegerField()),
                ('description', models.TextField()),
                ('code_postal', models.IntegerField()),
                ('type', models.CharField(max_length=32)),
                ('svg_id', models.CharField(max_length=16)),
            ],
            options={
                'db_table': 'circonscription',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Election',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('label', models.CharField(max_length=255)),
                ('status', models.IntegerField()),
                ('url', models.CharField(max_length=255)),
                ('pact_name', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'election',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ElectionCandidateLink',
            fields=[
                ('election_candidate_link_id', models.AutoField(serialize=False, primary_key=True)),
                ('finalist', models.BooleanField(default=None)),
                ('elected', models.BooleanField(default=None)),
                ('votes_1st_ballot', models.FloatField()),
                ('votes_2nd_ballot', models.FloatField()),
                ('decision', models.CharField(max_length=20)),
                ('candidate', models.ForeignKey(related_name='election_candidate_link_c', to='gpt2.Candidat')),
                ('constituency', models.ForeignKey(related_name='election_candidate_link', to='gpt2.Circonscription')),
                ('election', models.ForeignKey(related_name='election_candidate_link', to='gpt2.Election')),
            ],
            options={
                'db_table': 'election_candidate_link',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ElectionConstituencyLink',
            fields=[
                ('election_constituency_link_id', models.AutoField(serialize=False, primary_key=True)),
                ('constituency', models.ForeignKey(related_name='election_constituency_link', to='gpt2.Circonscription')),
                ('election', models.ForeignKey(related_name='election_constituency_link', to='gpt2.Election')),
            ],
            options={
                'db_table': 'election_constituency_link',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Parti',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('acronyme', models.CharField(max_length=16)),
                ('nom_complet', models.CharField(max_length=64)),
                ('description', models.TextField()),
            ],
            options={
                'db_table': 'parti',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Volontaire',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('prenom', models.CharField(max_length=255)),
                ('nom', models.CharField(max_length=255)),
                ('login', models.CharField(max_length=255)),
                ('email', models.CharField(max_length=255)),
                ('email_visible', models.BooleanField(default=None)),
                ('email_valide', models.BooleanField(default=None)),
                ('cle', models.CharField(max_length=41)),
                ('code_postal', models.CharField(max_length=10)),
                ('ville', models.CharField(max_length=255)),
                ('tel', models.CharField(max_length=20)),
                ('commentaire', models.CharField(max_length=255)),
                ('user', models.OneToOneField(default=None, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'volontaire',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='candidat',
            name='constituencies',
            field=models.ManyToManyField(related_name='candidates', through='gpt2.ElectionCandidateLink', to='gpt2.Circonscription'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='candidat',
            name='parti',
            field=models.ForeignKey(related_name='candidat', to='gpt2.Parti'),
            preserve_default=True,
        ),
    ]

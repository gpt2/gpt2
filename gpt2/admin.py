from gpt2.models import *
from django.contrib import admin

class CandidatAdmin(admin.ModelAdmin):
    list_display = ['nom', 'prenom', 'parti']
    fields = ['prenom', 'nom', 'parti', 'photo']
    list_filter = ['parti']
    search_fields = ['nom']
    ordering = ['nom','prenom']

admin.site.register(Candidat, CandidatAdmin)

admin.site.register(Volontaire)

admin.site.register(Parti)

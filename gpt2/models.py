from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from utils import *
import os

def get_image_path(instance, filename):
    return 'photos/candidate_%s.png' % str(instance.id)

class Parti(models.Model):
    class Meta:
        db_table = 'parti'

    acronyme = models.CharField(max_length=16)
    nom_complet = models.CharField(max_length=64)
    description = models.TextField()

    def __unicode__(self):
        if self.acronyme:
            return "%s (%s)" % ( self.nom_complet, self.acronyme )
        else:
            return self.nom_complet



class Election(models.Model):
    class Meta:
        db_table = 'election'

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    label = models.CharField(max_length=255)
    status = models.IntegerField()
    url = models.CharField(max_length=255)
    pact_name = models.CharField(max_length=255)
    local_authority_type = models.CharField(max_length=255)



class Circonscription(models.Model):
    class Meta:
        db_table = 'circonscription'

    id = models.AutoField(primary_key=True)
    numero = models.IntegerField()
    description = models.TextField()
    code_postal = models.IntegerField()
    type = models.CharField(max_length=32)
    svg_id = models.CharField(max_length=16)

    def candidates_of_election(self, election_id):
        return self.candidates.filter(electioncandidatelink__election_id=election_id)



class Collectivite(models.Model):
    class Meta:
        db_table = 'collectivite'

    id = models.AutoField(primary_key=True)

    numero = models.IntegerField()
    description = models.TextField()
    type = models.CharField(max_length=32)
    svg_id = models.CharField(max_length=16)
    admin_info = models.TextField()
    order_num = models.IntegerField()



class CollectiviteLink(models.Model):
    class Meta:
        db_table = 'collectivite_link'

    id = models.AutoField(primary_key=True)
    child_type = models.TextField ()
    parent = models.OneToOneField ( Collectivite, 
                                    related_name = 'collectivite_link_parent')
    child = models.OneToOneField ( Circonscription, 
                                   related_name = 'collectivite_link_child' )



class ElectionConstituencyLink(models.Model):
    class Meta:
        db_table = 'election_constituency_link'

    election_constituency_link_id = models.AutoField(primary_key=True)
    constituency = models.ForeignKey ( Circonscription, related_name = 'election_constituency_link' )
    election = models.ForeignKey ( Election, related_name = 'election_constituency_link' )



class Candidat(models.Model):
    class Meta:
        db_table = 'candidat'

    parti = models.ForeignKey(Parti, related_name='candidat')
    civilite = models.CharField(max_length=255, null=True)
    prenom = models.CharField(max_length=255)
    nom = models.CharField(max_length=255)
    constituencies = models.ManyToManyField(Circonscription, through='ElectionCandidateLink', related_name='candidates')
    photo = models.ImageField(upload_to=get_image_path, blank=True, null=True)

    tel = models.CharField(max_length=255)
    fax = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    web = models.CharField(max_length=255)
    adresse = models.CharField(max_length=255)
    code_postal = models.CharField(max_length=20)
    ville = models.CharField(max_length=20)
    commentaire = models.CharField(max_length=255)
    commentaire_admin = models.CharField(max_length=255)


    # XXX howto add dynamic fields ?
    additional_position_in_list = models.IntegerField()
    additional_nom_liste = models.CharField(max_length=128)


    def constituencies_of_election(self, election_id):
        return self.constituencies.filter(election_candidate_link__election_id=election_id)

    def __unicode__(self):
        return "%s %s" % ( self.prenom, self.nom )

    def no_photo_url (self):
        if self.civilite == 'Mr':
            return 'images/candidate-no-photo-male.png'
        else:
            return 'images/candidate-no-photo-female.png'

class ElectionCandidateLink(models.Model):
    class Meta:
        db_table = 'election_candidate_link'

    election_candidate_link_id = models.AutoField(primary_key=True)
    candidate = models.ForeignKey(Candidat, related_name='election_candidate_link_c')
    constituency = models.ForeignKey(Circonscription, related_name='election_candidate_link')

    election = models.ForeignKey(Election, related_name='election_candidate_link')
    finalist = models.BooleanField(default=None)
    elected = models.BooleanField(default=None)

    votes_1st_ballot = models.FloatField()
    votes_2nd_ballot = models.FloatField()

    decision = models.CharField(max_length=20)



class Volontaire(models.Model):
    class Meta:
        db_table = 'volontaire'

    id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User, default=None, on_delete=models.CASCADE)
    login = models.CharField(max_length=255)
    passwd = models.CharField(max_length=255)

    photo = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    prenom = models.CharField(max_length=255)
    nom = models.CharField(max_length=255)

    email = models.CharField(max_length=255)
    email_visible = models.BooleanField(default=None)
    email_valide = models.BooleanField(default=None)
    cle = models.CharField(max_length=41)

    code_postal = models.CharField(max_length=10)
    ville = models.CharField(max_length=255)
    tel = models.CharField(max_length=20)
    commentaire = models.CharField(max_length=255)

    def __unicode__(self):
        return "%s %s" % ( self.prenom, self.nom )


class Responsabilite(models.Model):
    class Meta:
        db_table = 'responsabilite'

    id = models.AutoField(primary_key=True)

    volontaire = models.ForeignKey(Volontaire,related_name='responsabilites')

    type = models.ForeignKey(ContentType, db_column='type')
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('type', 'object_id')

    validation = models.BooleanField()
    auteur_validation = models.CharField(max_length=20)
    date_validation = models.DateField()

    election = models.ForeignKey(Election)

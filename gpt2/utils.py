from django.template import RequestContext, loader
from django.core.urlresolvers import resolve
from django.shortcuts import render, render_to_response, get_object_or_404

import gpt2.models


def get_election_id_from_request ( request ):
    return get_election_from_request ( request ) . id

def get_election_from_request ( request ):
    app_name = resolve ( request.path_info ) . namespace
    election = gpt2.models.Election.objects.filter(name = app_name)
    return election [ 0 ]


def add_namespace_processor(request):
    #logging.warning(resolve ( request.path_info ) . app_name)
    return {'current_app': resolve ( request.path_info ) . app_name}


def gpt2_render_to_response ( request, template, additional ):
    app_name = resolve ( request.path_info ) . namespace

    if app_name and len(app_name) > 0:
        additional [ 'base_template' ] = '%s/base_%s.html' % ( app_name, app_name )
        additional [ 'current_app' ] = app_name
        additional [ 'election' ] = get_election_from_request ( request )
    else:
        additional [ 'current_app' ] = 'gpt2'
        additional [ 'base_template' ] = 'base.html'

    t = loader.get_template(template)
    c = RequestContext(request, additional, [add_namespace_processor], current_app=app_name)

    return render ( request, template, additional, context_instance = c )


def set_debug ( ):
    import logging
    l = logging.getLogger('django.db.backends')
    l.setLevel(logging.DEBUG)
    l.addHandler(logging.StreamHandler())

from django.conf import settings
from django.db.models import Q, Count
from django.http import HttpResponse, Http404
from django.contrib.auth.decorators import login_required
from django_tables2 import RequestConfig

from gpt2.models import *
from gpt2.tables import *
from gpt2.utils import *



def home ( request ):
    return gpt2_render_to_response ( request, 'home.html', { 'elections': Election.objects.all() } )


def index ( request ):
    eid = get_election_id_from_request ( request )

    count = ElectionCandidateLink.objects.filter ( Q(election_id = eid),
                                                   ( Q(decision = 'signe') | 
                                                     Q(decision = 'signe-valide' ) ) ).count

    parties = Parti.objects \
        .filter ( Q ( candidat__election_candidate_link_c__election_id = eid ), 
                  ( Q ( candidat__election_candidate_link_c__decision = 'signe-valide' ) | 
                    Q ( candidat__election_candidate_link_c__decision = 'signe') ) ) \
                    .annotate ( count = Count ( 'id' ) ) \
                    .order_by ( '-count' )

    return gpt2_render_to_response(request, 'index.html', { 'count' : count, 'parties' : parties } )


def view_profile(request, id = None):
    if id is None:
        id = request.user.id

    # XXX Dangerous, check I am admin
    volontaire = get_object_or_404(Volontaire, pk=id)

    return gpt2_render_to_response ( 
        request, 'volontaire.html', { 'volontaire': volontaire } )


def candidate_detail(request, id):
    candidat = get_object_or_404(Candidat, pk=id)

    return gpt2_render_to_response ( 
        request, 'candidat.html', { 'candidat': candidat } )


def constituencies_list ( request ):
    election = get_election_from_request ( request )

    constituencies = Circonscription.objects \
        .filter ( election_constituency_link__election_id = election.id ) \
        .filter ( collectivite_link_child__parent__type = election.local_authority_type ) \
        .order_by ( 'collectivite_link_child__parent__order_num', 
                    'collectivite_link_child__parent__numero', 
                    'collectivite_link_child__parent__description' ) \
        .prefetch_related ( 'collectivite_link_child__parent' )
                    
    return gpt2_render_to_response ( request, 'constituencies.html', 
                                     { 'constituencies': constituencies } )


def constituency_detail(request, id):
    eid = get_election_id_from_request ( request )
    the_constituency = get_object_or_404(Circonscription, pk=id)

    candidates = the_constituency.election_candidate_link \
        . filter ( election_id = eid ) \
        . order_by ( '-votes_1st_ballot', 'candidate__additional_nom_liste', 'candidate__additional_position_in_list', 'candidate__nom', 'candidate__prenom' ) \
        . prefetch_related ( 'candidate', 'candidate__parti' )

    finalists = the_constituency.election_candidate_link \
        . filter ( election_id = eid, finalist = 1 ) \
        . order_by ( '-elected', '-votes_2nd_ballot',  'candidate__additional_nom_liste', 'candidate__additional_position_in_list', 'candidate__nom', 'candidate__prenom' , '-votes_1st_ballot' ) \
        . prefetch_related ( 'candidate', 'candidate__parti' )

    other_elections = ElectionConstituencyLink.objects \
        .filter ( constituency = the_constituency ) \
        .exclude ( election_id = eid )
                                                                
    return gpt2_render_to_response ( request, 'constituency.html', 
                                     { 'constituency': the_constituency, 
                                       'candidates': candidates,
                                       'finalists': finalists,
                                       'other_elections': other_elections,
                                       'election_id': eid })


def signatories_list(request):
    request.GET = request.GET.copy ()
    request.GET [ 'signatories' ] = 1
    return candidates_list ( request )


def candidates_list(request):
    e = get_election_from_request ( request )
    cdt = Candidat.objects \
        . filter ( Q(election_candidate_link_c__election_id = e.id ) ) \
        . order_by ( 'nom', 'prenom' )
                         
    title = 'Candidats'

    if request.GET.has_key ( 'signatories' ):
        cdt = cdt . filter ( Q(election_candidate_link_c__decision = 'signe') | 
                             Q(election_candidate_link_c__decision = 'signe-valide' ) )
        title = 'Signatories'

    if request.GET.has_key ( 'party' ):
        cdt = cdt . filter ( parti_id = request.GET [ 'party' ] )
        title = '%s for %s' % ( title, Parti.objects.get(pk=request.GET [ 'party' ]) )

    cdt = cdt . distinct ( ) . prefetch_related ( 'parti' )

    table = CandidateTable(cdt, election=e)
    RequestConfig(request).configure(table)
                                                                
    return gpt2_render_to_response ( request, 'candidates.html', 
                                     { 'candidates': table,
                                       'title': title,
                                       'election_id': e.id } )

from django.conf.urls import patterns, url
from gpt2 import views
import gpt2.auth.views

urlpatterns = patterns (
    'gpt2.views',
    url(r'^/?$', views.index, name='index'),

    url(r'^login', gpt2.auth.views.login_form, name='login'),
    url(r'^logout', gpt2.auth.views.logout_form, name='logout'),

    url(r'^constituency/(?P<id>\d+)/?$', views.constituency_detail, name='constituency_detail'),
    url(r'^candidate/(?P<id>\d+)/?$', views.candidate_detail, name='candidate_detail'),

    url(r'^profile/(?P<id>\d+)$', views.view_profile, name='view_profile'),
    url(r'^profile/?$', views.view_profile, name='view_profile'),

    url(r'^candidates$', views.candidates_list, name='candidates_list'),
    url(r'^signatories$', views.signatories_list, name='signatories_list'),
    url(r'^constituencies$', views.constituencies_list, name='constituencies_list'),

    )

from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

import django_tables2 as tables
from django_tables2.utils import A 

from pprint import pformat


class PhotoColumn(tables.Column):

    def __init__(self, args=None, **kwargs):
        kwargs["empty_values"] = ()
        self._args = args
        super(PhotoColumn, self).__init__(**kwargs)

    def render(self, value, record, bound_column):
        args = [a.resolve(record) if isinstance(a, A) else a
                for a in self._args]
        if args[0] == 'Mr':
            gender = 'male'
        else:
            gender = 'female'

        if value:
            return mark_safe('<span class="candidate-photo" style="background-image:url(\'%s\')"></span>' % ( settings.MEDIA_URL + str(value) ))
        else:
            return mark_safe('<span class="candidate-photo" style="background-image:url(\'%s/images/candidate-no-photo-%s.png\')"></span>' % ( settings.STATIC_URL, gender ))



class CandidateTable(tables.Table):

    def __init__(self, args=None, **kwargs):
        self._election = kwargs['election']
        super(CandidateTable, self).__init__(args)

    photo = PhotoColumn(args=[A('civilite')])
    prenom = tables.LinkColumn('gpt2:candidate_detail', args=[A('pk')])
    nom = tables.LinkColumn('gpt2:candidate_detail', args=[A('pk')])
    parti = tables.Column()

    def render_candidate_link (self, **kwargs):
        return \
            ( '<a href="%s">%s</a>' % \
                  ( reverse ( self._election.name + ':candidate_detail', 
                              args=[kwargs['record'].id] ), 
                    kwargs['value'] ) )

    def render_prenom (self, **kwargs):
        return mark_safe ( self.render_candidate_link ( **kwargs ) )

    def render_nom (self, **kwargs):
        return mark_safe ( self.render_candidate_link ( **kwargs ) )

    class Meta:
        attrs = {"class": "table table-hover candidate-table"}

from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^/?$', 'gpt2.views.home', name='home'),

    url(r'^regionales2010/', include('gpt2.urls', namespace='regionales2010', app_name='gpt2')),
    url(r'^cantonales2008/', include('gpt2.urls', namespace='cantonales2008', app_name='gpt2')),
    url(r'^europarl2009/', include('gpt2.urls', namespace='europarl2009', app_name='gpt2')),
    url(r'^municipales2008/', include('gpt2.urls', namespace='municipales2008', app_name='gpt2')),
    url(r'^municipales2014/', include('gpt2.urls', namespace='municipales2014', app_name='gpt2')),
    url(r'^cantonales2011/', include('gpt2.urls', namespace='cantonales2011', app_name='gpt2')),
    url(r'^legislatives2012/', include('gpt2.urls', namespace='legislatives2012', app_name='gpt2')),
    url(r'^legislatives2007/', include('gpt2.urls', namespace='legislatives2007', app_name='gpt2')),
    url(r'^initiatives/', include('gpt2.urls', namespace='initiatives', app_name='gpt2')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
